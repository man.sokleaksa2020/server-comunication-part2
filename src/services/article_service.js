import api from "../api/api"

export const fetchArticle = async () => {
    let response = await api.get('articles')
    return response.data.data
}

export const deleteArticle = async (id) => {
    let response = await api.delete('articles/' + id)
    return response.data.message
}

export const postArticle = async (article) => {
    let response = await api.post('articles', article)
    return response.data.message
}

export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await api.post('images', formData)
    return response.data.url
}